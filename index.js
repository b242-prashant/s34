// Using require directive to load express module/package

const express = require("express");

// Create an application using express
const app = express();

// The port where the app will listen to 
const port = 3000;

//Allow the server to handle data from request
//Allows the app to read json data
app.use(express.json());


//Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

app.get("/",(req,res) => {
	res.send("Hello world!");
})

app.get("/hello",(req,res) => {
	res.send("Hello from the endpoint");
})

app.post("/hello", (req,res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

// Now we will create a mock database
let users = [];

app.post("/signup",(req,res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		// Stores the user object to the users array created
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please enter a valid username or password");
	}
})

app.listen(port, () => console.log(`Server running at port ${port}`));
